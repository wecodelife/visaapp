import 'dart:ui';

class Constants {
  static List<Color> kitGradients = [
    Color(0xFFE5E5E5),
    Color(0xFFFFFFFF),
    Color(0xFF6C6C6C),
    Color(0xFF016BA6),
    Color(0xFF5E5E5E),
    Color(0xFFF0EFEF), //5
    Color(0xFFC4C4C4),
    Color(0xFFEEEEEE),
  ];
}
