import 'dart:async';


import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:visa_ui/models/display_name_request_model.dart';
import 'package:visa_ui/models/login_request_model.dart';
import 'package:visa_ui/utils/urls.dart';

import 'object_factory.dart';

class ApiClient {
  ApiClient() {
    initClient();
  }

//for api client testing only
  ApiClient.test({@required this.dio});

  Dio dio;



  initClient() async {

    dio = Dio();


    dio.interceptors.add(InterceptorsWrapper(
      onRequest: (RequestOptions reqOptions) {
        return reqOptions;
      },
      onError: (DioError dioError) {
        return dioError.response;
      },
    ));
  }

///login user
  Future<Response> login(LoginRequestModel loginRequestModel) {
    return dio.post(Urls.loginUrl, data: loginRequestModel);
  }
// ///display name
//   Future<Response> updateName(DisplayNameRequestModel displayNameRequestModel) {
//     dio.options.headers
//         .addAll({"Authorization":"Token "+ObjectFactory().appHive.getToken()});
//     return dio.post(Urls.displayNameUrl, data: displayNameRequestModel);
//   }

  // /// search user
  // Future<Response> sampleCall() {
  //   dio.options.headers
  //       .addAll({"Authorization": ObjectFactory().prefs.getAuthToken()});
  //   return dio.get("");
  // }
}
