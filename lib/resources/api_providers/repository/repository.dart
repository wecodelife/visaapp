

import 'package:visa_ui/models/display_name_request_model.dart';
import 'package:visa_ui/models/login_request_model.dart';
import 'package:visa_ui/models/state.dart';

import '../user_api_provider.dart';

/// Repository is an intermediary class between network and data
class Repository {
  final userApiProvider = UserApiProvider();

  Future<State> login({LoginRequestModel loginRequestModel}) =>
      userApiProvider.login(loginRequestModel);
  // Future<State> updateName({DisplayNameRequestModel displayNameRequestModel}) =>
  //     userApiProvider.updateName(displayNameRequestModel);
}
