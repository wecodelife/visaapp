



import 'package:visa_ui/models/display_name_request_model.dart';
import 'package:visa_ui/models/display_name_response_model.dart';
import 'package:visa_ui/models/login_request_model.dart';
import 'package:visa_ui/models/login_response_model.dart';
import 'package:visa_ui/models/state.dart';
import 'package:visa_ui/utils/object_factory.dart';

class UserApiProvider {



  Future<State> login(LoginRequestModel loginRequestModel) async {
    final response = await ObjectFactory().apiClient.login(loginRequestModel);
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode==201) {
      return State<LoginResponseModel>.success(
          LoginResponseModel.fromJson(response.data));
    } else {
      return State<LoginResponseModel>.error(
          LoginResponseModel.fromJson(response.data));
    }
  }

  // Future<State> updateName(DisplayNameRequestModel displayNameRequestModel) async {
  //   final response = await ObjectFactory().apiClient.updateName(displayNameRequestModel);
  //   print(response.toString());
  //   if (response.statusCode == 200 || response.statusCode==201) {
  //     return State<DisplayNameResponseModel>.success(
  //         DisplayNameResponseModel.fromJson(response.data));
  //   } else {
  //     return State<DisplayNameResponseModel>.error(
  //         DisplayNameResponseModel.fromJson(response.data));
  //   }
  // }


}
