import 'package:flutter/material.dart';
import 'package:visa_ui/screens/about_us.dart';
import 'package:visa_ui/screens/apply_for_visa.dart';
import 'package:visa_ui/screens/check_status.dart';
import 'package:visa_ui/screens/contact_us.dart';
import 'package:visa_ui/screens/my_uae_visa.dart';
import 'package:visa_ui/screens/news_and_updates.dart';
import 'package:visa_ui/screens/terms_and_conditions.dart';
import 'package:visa_ui/utils/util.dart';

class HomeDrawer extends StatefulWidget {
  @override
  _HomeDrawerState createState() => _HomeDrawerState();
}

class _HomeDrawerState extends State<HomeDrawer> {
  @override
  Widget build(BuildContext context) {
    return new Drawer(
      child: ListView(
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: screenWidth(context, dividedBy: 12),
                      vertical: screenHeight(context, dividedBy: 18)),
                  child: Column(children: [
                    Text(
                      'My UAE Visa',
                      style: TextStyle(
                        fontFamily: 'ProximaNova',
                        fontSize: 25.0,
                        color: Color(0xFF016BA6),
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                    Text(
                      'powered by smart travels',
                      style: TextStyle(
                        fontFamily: 'ProximaNova',
                        fontSize: 12.0,
                        color: Color(0xFF6C6C6C),
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ])),
              Container(
                height: 0.5,
                color: Colors.grey,
              ),
              Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: screenWidth(context, dividedBy: 12),
                    vertical: screenHeight(context, dividedBy: 20)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    GestureDetector(
                      child: Container(
                        color: Colors.transparent,
                        width: screenWidth(context, dividedBy: 1.5),
                        height: screenHeight(context, dividedBy: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              'My UAE Visa',
                              style: TextStyle(
                                fontFamily: 'ProximaNova',
                                fontSize: 14.0,
                                color: Color(0xFF5E5E5E),
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                          ],
                        ),
                      ),
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => MyUAEVisa()));
                      },
                    ),
                    GestureDetector(
                      child: Container(
                        color: Colors.transparent,
                        width: screenWidth(context, dividedBy: 1.5),
                        height: screenHeight(context, dividedBy: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              'Check your Visa',
                              style: TextStyle(
                                fontFamily: 'ProximaNova',
                                fontSize: 14.0,
                                color: Color(0xFF5E5E5E),
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                          ],
                        ),
                      ),
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => Checkstatus()));
                      },
                    ),
                    GestureDetector(
                      child: Container(
                        color: Colors.transparent,
                        width: screenWidth(context, dividedBy: 1.5),
                        height: screenHeight(context, dividedBy: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              'Apply for Visa',
                              style: TextStyle(
                                fontFamily: 'ProximaNova',
                                fontSize: 14.0,
                                color: Color(0xFF5E5E5E),
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                          ],
                        ),
                      ),
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ApplyForVisa()));
                      },
                    ),
                    GestureDetector(
                      child: Container(
                        color: Colors.transparent,
                        height: screenHeight(context, dividedBy: 20),
                        width: screenWidth(context, dividedBy: 1.5),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              'Terms & Condition',
                              style: TextStyle(
                                fontFamily: 'ProximaNova',
                                fontSize: 14.0,
                                color: Color(0xFF5E5E5E),
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                          ],
                        ),
                      ),
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => TermsConditions()));
                      },
                    ),
                    GestureDetector(
                      child: Container(
                        color: Colors.transparent,
                        width: screenWidth(context, dividedBy: 1.5),
                        height: screenHeight(context, dividedBy: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              'News and update',
                              style: TextStyle(
                                fontFamily: 'ProximaNova',
                                fontSize: 14.0,
                                color: Color(0xFF5E5E5E),
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                          ],
                        ),
                      ),
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => NewsUpdates()));
                      },
                    ),
                    GestureDetector(
                      child: Container(
                        color: Colors.transparent,
                        width: screenWidth(context, dividedBy: 1.5),
                        height: screenHeight(context, dividedBy: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              'About us',
                              style: TextStyle(
                                fontFamily: 'ProximaNova',
                                fontSize: 14.0,
                                color: Color(0xFF5E5E5E),
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                          ],
                        ),
                      ),
                      onTap: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) => AboutUs()));
                      },
                    ),
                    GestureDetector(
                      child: Container(
                        color: Colors.transparent,
                        width: screenWidth(context, dividedBy: 1.5),
                        height: screenHeight(context, dividedBy: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              'Contact us',
                              style: TextStyle(
                                fontFamily: 'ProximaNova',
                                fontSize: 14.0,
                                color: Color(0xFF5E5E5E),
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                          ],
                        ),
                      ),
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ContactUs()));
                      },
                    ),
                    GestureDetector(
                      child: Container(
                        color: Colors.transparent,
                        width: screenWidth(context, dividedBy: 1.5),
                        height: screenHeight(context, dividedBy: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              'Language',
                              style: TextStyle(
                                fontFamily: 'ProximaNova',
                                fontSize: 14.0,
                                color: Color(0xFF5E5E5E),
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                          ],
                        ),
                      ),
                      onTap: () {},
                    ),
                  ],
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}
