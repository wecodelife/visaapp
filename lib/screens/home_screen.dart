import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:visa_ui/utils/constants.dart';
import 'package:visa_ui/utils/util.dart';
import 'package:visa_ui/widgets/home_drawer.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();
  ScrollController _controller = new ScrollController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      key: _globalKey,
      drawer: HomeDrawer(),
      appBar: AppBar(
        backgroundColor: Constants.kitGradients[3],
        toolbarHeight: screenHeight(context, dividedBy: 15),
        leading: GestureDetector(
          child: Container(
              color: Constants.kitGradients[3],
              height: screenHeight(context, dividedBy: 15),
              child: Icon(Icons.menu)),
          onTap: () {
            _globalKey.currentState.openDrawer();
          },
        ),
      ),
      backgroundColor: Constants.kitGradients[5],
      body: SingleChildScrollView(
          child: ListView.builder(
        controller: _controller,
        shrinkWrap: true,
        padding: EdgeInsets.only(
            left: screenWidth(context, dividedBy: 40),
            right: screenWidth(context, dividedBy: 40)),
        scrollDirection: Axis.vertical,
        itemCount: 25,
        itemBuilder: (BuildContext cntxt, int index) {
          return Padding(
            padding: EdgeInsets.only(top: screenHeight(context, dividedBy: 40)),
            child: Container(
              width: screenWidth(context, dividedBy: 1.6),
              height: screenHeight(context, dividedBy: 5),
              decoration: BoxDecoration(
                color: Constants.kitGradients[0],
                borderRadius: BorderRadius.circular(10),
              ),
              child: Row(
                children: [
                  Container(
                      child: Padding(
                        padding: EdgeInsets.symmetric(
                            vertical: screenHeight(context, dividedBy: 28),
                            horizontal: screenWidth(context, dividedBy: 35)),
                        child: Image.asset('assets/images/uae_icon.png'),
                      ),
                      width: screenWidth(context, dividedBy: 5.8),
                      height: screenHeight(context, dividedBy: 5.0),
                      decoration: BoxDecoration(
                        color: Constants.kitGradients[3],
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            bottomLeft: Radius.circular(10)),
                      )),
                  Padding(
                    padding: EdgeInsets.symmetric(
                        vertical: screenHeight(context, dividedBy: 70),
                        horizontal: screenWidth(context, dividedBy: 35)),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          width: screenWidth(context, dividedBy: 1.4),
                          child: Text(
                            'Visa change inside the country with insurance',
                            overflow: TextOverflow.visible,
                            style: TextStyle(
                              fontFamily: 'ProximaNova',
                              fontSize: 18.0,
                              color: Constants.kitGradients[3],
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                        ),
                        SizedBox(height: screenHeight(context, dividedBy: 180)),
                        Container(
                          width: screenWidth(context, dividedBy: 1.4),
                          child: Text(
                            'Inside country only',
                            overflow: TextOverflow.visible,
                            style: TextStyle(
                              fontFamily: 'ProximaNova',
                              fontSize: 14.0,
                              color: Constants.kitGradients[2],
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ),
                        SizedBox(height: screenHeight(context, dividedBy: 180)),
                        Container(
                          width: screenWidth(context, dividedBy: 1.4),
                          child: Text(
                            'Covid-19 insurance',
                            overflow: TextOverflow.visible,
                            style: TextStyle(
                              fontFamily: 'ProximaNova',
                              fontSize: 14.0,
                              color: Constants.kitGradients[2],
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ),
                        SizedBox(height: screenHeight(context, dividedBy: 32)),
                        Container(
                          width: screenWidth(context, dividedBy: 1.4),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Container(
                                width: screenWidth(context, dividedBy: 4.0),
                                height: screenHeight(context, dividedBy: 35),
                                decoration: BoxDecoration(
                                    color: Color(0xFF016BA6),
                                    borderRadius: BorderRadius.circular(18)),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      '1500',
                                      style: TextStyle(
                                        fontFamily: 'ProximaNova',
                                        fontSize: 16.0,
                                        color: Constants.kitGradients[1],
                                        fontWeight: FontWeight.w700,
                                      ),
                                    ),
                                    Text(
                                      ' AED',
                                      style: TextStyle(
                                        fontFamily: 'ProximaNova',
                                        fontSize: 16.0,
                                        color: Constants.kitGradients[1],
                                        fontWeight: FontWeight.w400,
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      )),
    );
  }
}
