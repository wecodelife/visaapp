import 'package:flutter/material.dart';
import 'package:visa_ui/utils/constants.dart';
import 'package:visa_ui/utils/util.dart';

import 'home_screen.dart';

class TermsConditions extends StatefulWidget {
  @override
  _TermsConditionsState createState() => _TermsConditionsState();
}

class _TermsConditionsState extends State<TermsConditions> {
  GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _globalKey,
      backgroundColor: Constants.kitGradients[1],
      appBar: AppBar(
        leading: GestureDetector(
          child: Container(
            width: screenWidth(context, dividedBy: 10),
            color: Color(0xFF016BA6),
            child: Icon(
              Icons.arrow_back_ios,
              color: Constants.kitGradients[1],
              size: 18,
            ),
          ),
          onTap: () {
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(builder: (context) => HomeScreen()),
                (route) => false);
          },
        ),
        toolbarHeight: screenHeight(context, dividedBy: 15),
        backgroundColor: Constants.kitGradients[3],
        title: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text(
              'Terms & Conditions',
              style: TextStyle(
                fontFamily: 'ProximaNova',
                fontSize: 16.0,
                color: Constants.kitGradients[1],
                fontWeight: FontWeight.w700,
              ),
            ),
          ],
        ),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(
              height: screenHeight(context, dividedBy: 40),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  'Acceptance of Terms & Conditions',
                  overflow: TextOverflow.visible,
                  style: TextStyle(
                    fontFamily: 'ProximaNova',
                    fontSize: 16.0,
                    color: Constants.kitGradients[4],
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ],
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 60),
            ),
            Container(
              width: screenWidth(context, dividedBy: 1.0),
              child: Text(
                '    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                overflow: TextOverflow.visible,
                style: TextStyle(
                  fontFamily: 'ProximaNova',
                  fontSize: 12.0,
                  color: Constants.kitGradients[4],
                  fontWeight: FontWeight.w400,
                ),
              ),
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 40),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  'Overview',
                  overflow: TextOverflow.visible,
                  style: TextStyle(
                    fontFamily: 'ProximaNova',
                    fontSize: 16.0,
                    color: Constants.kitGradients[4],
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ],
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 60),
            ),
            Container(
              width: screenWidth(context, dividedBy: 1.0),
              child: Text(
                '        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                overflow: TextOverflow.visible,
                style: TextStyle(
                  fontFamily: 'ProximaNova',
                  fontSize: 12.0,
                  color: Constants.kitGradients[4],
                  fontWeight: FontWeight.w400,
                ),
              ),
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 40),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  'General',
                  overflow: TextOverflow.visible,
                  style: TextStyle(
                    fontFamily: 'ProximaNova',
                    fontSize: 16.0,
                    color: Constants.kitGradients[4],
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ],
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 60),
            ),
            Container(
              width: screenWidth(context, dividedBy: 1.0),
              child: Text(
                '    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                overflow: TextOverflow.visible,
                style: TextStyle(
                  fontFamily: 'ProximaNova',
                  fontSize: 12.0,
                  color: Constants.kitGradients[4],
                  fontWeight: FontWeight.w400,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
