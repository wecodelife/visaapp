import 'package:flutter/material.dart';
import 'package:visa_ui/utils/constants.dart';
import 'package:visa_ui/utils/util.dart';
import 'package:visa_ui/widgets/text_field.dart';

import 'home_screen.dart';

class ContactUs extends StatefulWidget {
  @override
  _ContactUsState createState() => _ContactUsState();
}

class _ContactUsState extends State<ContactUs> {
  GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _globalKey,
      resizeToAvoidBottomPadding: false,
      backgroundColor: Constants.kitGradients[1],
      appBar: AppBar(
        leading: GestureDetector(
          child: Container(
            width: screenWidth(context, dividedBy: 10),
            color: Constants.kitGradients[3],
            child: Icon(
              Icons.arrow_back_ios,
              color: Constants.kitGradients[1],
              size: 18,
            ),
          ),
          onTap: () {
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(builder: (context) => HomeScreen()),
                (route) => false);
          },
        ),
        toolbarHeight: screenHeight(context, dividedBy: 15),
        backgroundColor: Constants.kitGradients[3],
        title: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text(
              'Contact Us',
              style: TextStyle(
                fontFamily: 'ProximaNova',
                fontSize: 16.0,
                color: Constants.kitGradients[1],
                fontWeight: FontWeight.w700,
              ),
            ),
          ],
        ),
      ),
      body: Stack(
        children: [
          Positioned(
            bottom: 0,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Container(
                  width: screenWidth(context, dividedBy: 1),
                  height: screenHeight(context, dividedBy: 2.1),
                  child: Image.asset('assets/images/contact_us_icon.png',
                      fit: BoxFit.fill),
                ),
              ],
            ),
          ),
          Positioned(
            bottom: screenHeight(context, dividedBy: 9.3),
            right: screenWidth(context, dividedBy: 3.2),
            child: Container(
              width: screenWidth(context, dividedBy: 2.7),
              height: screenHeight(context, dividedBy: 15),
              decoration: BoxDecoration(
                  color: Color(0xFF016BA6),
                  borderRadius: BorderRadius.circular(21)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'Submit now',
                    style: TextStyle(
                      fontFamily: 'ProximaNova',
                      fontSize: 16.0,
                      color: Constants.kitGradients[1],
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ],
              ),
            ),
          ),
          Positioned(
              top: 0,
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                child: TextInField(
                  title: 'Full name',
                  hintText: 'Enter full name',
                ),
              )),
          Positioned(
              top: screenHeight(context, dividedBy: 13.0),
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                child: TextInField(
                  title: 'Mobile number',
                  hintText: 'Enter Mobile number',
                ),
              )),
          Positioned(
              top: screenHeight(context, dividedBy: 6.4),
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                child: TextInField(
                  title: 'Email address',
                  hintText: 'Enter Email address',
                ),
              )),
          Positioned(
              top: screenHeight(context, dividedBy: 4.3),
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                child: TextInField(
                  title: 'Subject',
                  hintText: 'Enter Subject',
                ),
              )),
          Positioned(
              top: screenHeight(context, dividedBy: 3.2),
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                child: TextInField(
                  title: 'Message',
                  hintText: 'message',
                ),
              ))
        ],
      ),
    );
  }
}
