import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:visa_ui/utils/constants.dart';
import 'package:visa_ui/utils/util.dart';
import 'package:visa_ui/widgets/text_field.dart';

import 'home_screen.dart';

class Checkstatus extends StatefulWidget {
  @override
  _CheckstatusState createState() => _CheckstatusState();
}

class _CheckstatusState extends State<Checkstatus> {
  GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _globalKey,
      resizeToAvoidBottomPadding: false,
      backgroundColor: Constants.kitGradients[1],
      appBar: AppBar(
        leading: GestureDetector(
          child: Container(
            width: screenWidth(context, dividedBy: 10),
            color: Constants.kitGradients[3],
            child: Icon(
              Icons.arrow_back_ios,
              color: Constants.kitGradients[1],
              size: 18,
            ),
          ),
          onTap: () {
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(builder: (context) => HomeScreen()),
                (route) => false);
          },
        ),
        toolbarHeight: screenHeight(context, dividedBy: 15),
        backgroundColor: Constants.kitGradients[3],
        title: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text(
              'Check status',
              style: TextStyle(
                fontFamily: 'ProximaNova',
                fontSize: 16.0,
                color: Constants.kitGradients[1],
                fontWeight: FontWeight.w700,
              ),
            ),
          ],
        ),
      ),
      body: Stack(
        children: [
          Positioned(
            top: 0,
            child: Padding(
                padding: EdgeInsets.symmetric(
                    vertical: screenHeight(context, dividedBy: 15),
                    horizontal: screenWidth(context, dividedBy: 15)),
                child: Column(children: [
                  Text(
                    'My UAE Visa',
                    style: TextStyle(
                      fontFamily: 'ProximaNova',
                      fontSize: 25.0,
                      color: Constants.kitGradients[3],
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                  Text(
                    'powered by smart travels',
                    style: TextStyle(
                      fontFamily: 'ProximaNova',
                      fontSize: 12.0,
                      color: Constants.kitGradients[2],
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ])),
          ),
          Positioned(
            top: screenHeight(context, dividedBy: 6.5),
            child: Padding(
              padding: EdgeInsets.symmetric(
                  vertical: screenHeight(context, dividedBy: 65),
                  horizontal: screenWidth(context, dividedBy: 15)),
              child: Text(
                'To check and view your visa status, Please order number here',
                style: TextStyle(
                  fontFamily: 'ProximaNova',
                  fontSize: 12.0,
                  color: Constants.kitGradients[2],
                  fontWeight: FontWeight.w400,
                ),
              ),
            ),
          ),
          Positioned(
              top: screenHeight(context, dividedBy: 5.0),
              child: Container(
                width: screenWidth(context, dividedBy: 1),
                child: Padding(
                  padding: EdgeInsets.symmetric(
                      vertical: screenHeight(context, dividedBy: 32),
                      horizontal: screenWidth(context, dividedBy: 15)),
                  child: TextInField(
                    title: 'Order Number',
                    hintText: 'Enter Application number',
                  ),
                ),
              )),
          Positioned(
            bottom: 0,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Container(
                    width: screenWidth(context, dividedBy: 1),
                    child: Image.asset(
                      'assets/images/check_status_icon.png',
                      fit: BoxFit.fitWidth,
                    )),
              ],
            ),
          ),
          Positioned(
            bottom: screenHeight(context, dividedBy: 2.2),
            right: screenWidth(context, dividedBy: 3.2),
            child: Container(
              width: screenWidth(context, dividedBy: 2.7),
              height: screenHeight(context, dividedBy: 15),
              decoration: BoxDecoration(
                  color: Color(0xFF016BA6),
                  borderRadius: BorderRadius.circular(21)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'Check',
                    style: TextStyle(
                      fontFamily: 'ProximaNova',
                      fontSize: 16.0,
                      color: Constants.kitGradients[1],
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
