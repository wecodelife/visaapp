import 'dart:io';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:visa_ui/utils/constants.dart';
import 'package:visa_ui/utils/util.dart';
import 'package:visa_ui/widgets/text_field.dart';
import 'package:visa_ui/widgets/upload_file.dart';
import 'package:image_picker/image_picker.dart';
import 'home_screen.dart';


class ApplyForVisa extends StatefulWidget {
  @override
  _ApplyForVisaState createState() => _ApplyForVisaState();
}

class _ApplyForVisaState extends State<ApplyForVisa> {
  GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();
  File _image1,_image2,_image3,_image4,_image5;

  final picker = ImagePicker();

  Future getImage(int x) async {

    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    setState(() {
      if (pickedFile != null) {
        x==0?_image1 = File(pickedFile.path):x==1?_image2 = File(pickedFile.path):x==2?_image3 = File(pickedFile.path):x==3?_image4 = File(pickedFile.path):x==4?_image5 = File(pickedFile.path):null;
        print('Successfully selected image.'+_image1.toString());
      } else {
        print('No image selected.');
      }
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _globalKey,
      resizeToAvoidBottomPadding: false,
      backgroundColor: Constants.kitGradients[1],
      appBar: AppBar(
        leading: GestureDetector(
          child: Container(
            width: screenWidth(context, dividedBy: 10),
            color: Color(0xFF016BA6),
            child: Icon(
              Icons.arrow_back_ios,
              color: Constants.kitGradients[1],
              size: 18,
            ),
          ),
          onTap: () {
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(builder: (context) => HomeScreen()),
                (route) => false);
          },
        ),
        toolbarHeight: screenHeight(context, dividedBy: 15),
        backgroundColor: Constants.kitGradients[3],
        title: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text(
              'Apply for Visa',
              style: TextStyle(
                fontFamily: 'ProximaNova',
                fontSize: 16.0,
                color: Constants.kitGradients[1],
                fontWeight: FontWeight.w700,
              ),
            ),
          ],
        ),
      ),
      body: Stack(
        children: [
          Positioned(
              top: 0,
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                child: TextInField(
                  title: 'Full name',
                  hintText: 'Enter full name',
                ),
              )),
          Positioned(
              top: screenHeight(context, dividedBy: 13.0),
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                child: TextInField(
                  title: 'Email',
                  hintText: 'Enter Email address',
                ),
              )),
          Positioned(
              top: screenHeight(context, dividedBy: 6.4),
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                child: TextInField(
                  title: 'Country code',
                  hintText: 'choose code',
                ),
              )),
          Positioned(
              top: screenHeight(context, dividedBy: 4.3),
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                child: TextInField(
                  title: 'Date of entry',
                  hintText: 'Choose date of entry',
                ),
              )),
          Positioned(
              top: screenHeight(context, dividedBy: 3.2),
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                child: TextInField(
                  title: 'Nationality',
                  hintText: 'Choose nationality',
                ),
              )),
          Positioned(
            top: screenHeight(context, dividedBy: 2.28),
            child: Container(
                width: screenWidth(context, dividedBy: 1),
                height: screenHeight(context, dividedBy: 2.3),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        GestureDetector(
                          child: _image1 == null?UploadFiles(
                            title1: "Passport Page 1",
                            title2: 'tap to upload',
                          ):Stack(
                            children: [
                              Positioned(
                                child: Container(
                                  width: screenWidth(context, dividedBy: 2.2),
                                  height: screenHeight(context, dividedBy: 7.5),
                                  decoration: BoxDecoration(
                                      color: Constants.kitGradients[7],
                                      borderRadius: BorderRadius.circular(10)
                                  ),
                                  child: Image.file(_image1,fit: BoxFit.fill,),
                                ),
                              ),
                              Positioned(
                                  bottom: 0,
                                  right: 0,
                                  child: Container(
                                      width: screenWidth(context, dividedBy: 10),
                                      height: screenHeight(context, dividedBy: 18),
                                      child: Icon(Icons.edit,color: Constants.kitGradients[1],)))
                            ],
                          ),
                          onTap: (){
                            getImage(0);
                          },
                        ),
                        GestureDetector(
                          child: _image2 == null?UploadFiles(
                            title1: "Passport Page 2",
                            title2: 'tap to upload',
                          ):Stack(
                            children: [
                              Positioned(
                                child: Container(
                                  width: screenWidth(context, dividedBy: 2.2),
                                  height: screenHeight(context, dividedBy: 7.5),
                                  decoration: BoxDecoration(
                                      color: Constants.kitGradients[7],
                                      borderRadius: BorderRadius.circular(10)
                                  ),
                                  child: Image.file(_image2,fit: BoxFit.fill,),
                                ),
                              ),
                              Positioned(
                                  bottom: 0,
                                  right: 0,
                                  child: Container(
                                      width: screenWidth(context, dividedBy: 10),
                                      height: screenHeight(context, dividedBy: 18),
                                      child: Icon(Icons.edit,color:Constants.kitGradients[1])))
                            ],
                          ),
                          onTap: (){
                            getImage(1);
                          },
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        GestureDetector(
                          child: _image3 == null?UploadFiles(
                            title1: "Passport Page 3",
                            title2: 'tap to upload',
                          ):Stack(
                            children: [
                              Positioned(
                                child: Container(
                                  width: screenWidth(context, dividedBy: 2.2),
                                  height: screenHeight(context, dividedBy: 7.5),
                                  decoration: BoxDecoration(
                                      color: Constants.kitGradients[7],
                                      borderRadius: BorderRadius.circular(10)
                                  ),
                                  child: Image.file(_image3,fit: BoxFit.fill,),
                                ),
                              ),
                              Positioned(
                                  bottom: 0,
                                  right: 0,
                                  child: Container(
                                      width: screenWidth(context, dividedBy: 10),
                                      height: screenHeight(context, dividedBy: 18),
                                      child: Icon(Icons.edit,color: Constants.kitGradients[1])))
                            ],
                          ),
                          onTap: (){
                            getImage(2);
                          },
                        ),
                        GestureDetector(
                          child: _image4 == null?UploadFiles(
                            title1: "Personal photo",
                            title2: 'tap to upload',
                          ):Stack(
                            children: [
                              Positioned(
                                child: Container(
                                  width: screenWidth(context, dividedBy: 2.2),
                                  height: screenHeight(context, dividedBy: 7.5),
                                  decoration: BoxDecoration(
                                      color: Constants.kitGradients[7],
                                      borderRadius: BorderRadius.circular(10)
                                  ),
                                  child: Image.file(_image4,fit: BoxFit.fill,),
                                ),
                              ),
                              Positioned(
                                  bottom: 0,
                                  right: 0,
                                  child: Container(
                                      width: screenWidth(context, dividedBy: 10),
                                      height: screenHeight(context, dividedBy: 18),
                                      child: Icon(Icons.edit,color: Constants.kitGradients[1])))
                            ],
                          ),
                          onTap: (){
                            getImage(3);
                          },
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        GestureDetector(
                          child: _image5 == null?UploadFiles(
                            title1: "Ticket copy",
                            title2: 'tap to upload',
                          ):Stack(
                            children: [
                              Positioned(
                                child: Container(
                                width: screenWidth(context, dividedBy: 2.2),
                                height: screenHeight(context, dividedBy: 7.5),
                                decoration: BoxDecoration(
                                    color: Constants.kitGradients[7],
                                    borderRadius: BorderRadius.circular(10)
                                ),
                                child: Image.file(_image5,fit: BoxFit.fill,),
                            ),
                              ),
                            Positioned(
                                bottom: 0,
                                right: 0,
                                child: Container(
                                    width: screenWidth(context, dividedBy: 10),
                                    height: screenHeight(context, dividedBy: 18),
                                    child: Icon(Icons.edit,color: Constants.kitGradients[1])))
                            ],
                          ),
                          onTap: (){
                            getImage(4);
                          },
                        ),
                        Container(
                          width: screenWidth(context, dividedBy: 2.2),
                          height: screenHeight(context, dividedBy: 7.5),
                        ),
                      ],
                    )
                  ],
                )),
          )
        ],
      ),
    );
  }
}
