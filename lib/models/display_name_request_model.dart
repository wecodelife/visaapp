// To parse this JSON data, do
//
//     final displayNameRequestModel = displayNameRequestModelFromJson(jsonString);

import 'dart:convert';

DisplayNameRequestModel displayNameRequestModelFromJson(String str) => DisplayNameRequestModel.fromJson(json.decode(str));

String displayNameRequestModelToJson(DisplayNameRequestModel data) => json.encode(data.toJson());

class DisplayNameRequestModel {
  DisplayNameRequestModel({
    this.uid,
    this.name,
  });

  final String uid;
  final String name;

  factory DisplayNameRequestModel.fromJson(Map<String, dynamic> json) => DisplayNameRequestModel(
    uid: json["uid"] == null ? null : json["uid"],
    name: json["name"] == null ? null : json["name"],
  );

  Map<String, dynamic> toJson() => {
    "uid": uid == null ? null : uid,
    "name": name == null ? null : name,
  };
}
