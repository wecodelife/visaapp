// To parse this JSON data, do
//
//     final displayNameResponseModel = displayNameResponseModelFromJson(jsonString);

import 'dart:convert';

DisplayNameResponseModel displayNameResponseModelFromJson(String str) => DisplayNameResponseModel.fromJson(json.decode(str));

String displayNameResponseModelToJson(DisplayNameResponseModel data) => json.encode(data.toJson());

class DisplayNameResponseModel {
  DisplayNameResponseModel({
    this.message,
    this.status,
  });

  final String message;
  final int status;

  factory DisplayNameResponseModel.fromJson(Map<String, dynamic> json) => DisplayNameResponseModel(
    message: json["message"] == null ? null : json["message"],
    status: json["status"] == null ? null : json["status"],
  );

  Map<String, dynamic> toJson() => {
    "message": message == null ? null : message,
    "status": status == null ? null : status,
  };
}
