// To parse this JSON data, do
//
//     final loginResponseModel = loginResponseModelFromJson(jsonString);

import 'dart:convert';

LoginResponseModel loginResponseModelFromJson(String str) => LoginResponseModel.fromJson(json.decode(str));

String loginResponseModelToJson(LoginResponseModel data) => json.encode(data.toJson());

class LoginResponseModel {
  LoginResponseModel({
    this.message,
    this.status,
    this.data,
  });

  final String message;
  final int status;
  final Data data;

  factory LoginResponseModel.fromJson(Map<String, dynamic> json) => LoginResponseModel(
    message: json["message"] == null ? null : json["message"],
    status: json["status"] == null ? null : json["status"],
    data: json["data"] == null ? null : Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "message": message == null ? null : message,
    "status": status == null ? null : status,
    "data": data == null ? null : data.toJson(),
  };
}

class Data {
  Data({
    this.token,
    this.userData,
  });

  final String token;
  final UserData userData;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    token: json["token"] == null ? null : json["token"],
    userData: json["user_data"] == null ? null : UserData.fromJson(json["user_data"]),
  );

  Map<String, dynamic> toJson() => {
    "token": token == null ? null : token,
    "user_data": userData == null ? null : userData.toJson(),
  };
}

class UserData {
  UserData({
    this.id,
    this.name,
    this.phoneNumber,
    this.uid,
    this.userType,
  });

  final int id;
  final dynamic name;
  final String phoneNumber;
  final String uid;
  final UserType userType;

  factory UserData.fromJson(Map<String, dynamic> json) => UserData(
    id: json["id"] == null ? null : json["id"],
    name: json["name"],
    phoneNumber: json["phone_number"] == null ? null : json["phone_number"],
    uid: json["uid"] == null ? null : json["uid"],
    userType: json["user_type"] == null ? null : UserType.fromJson(json["user_type"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "name": name,
    "phone_number": phoneNumber == null ? null : phoneNumber,
    "uid": uid == null ? null : uid,
    "user_type": userType == null ? null : userType.toJson(),
  };
}

class UserType {
  UserType({
    this.id,
    this.isActive,
    this.isDeleted,
    this.createdAt,
    this.updatedAt,
    this.name,
  });

  final int id;
  final bool isActive;
  final bool isDeleted;
  final DateTime createdAt;
  final DateTime updatedAt;
  final String name;

  factory UserType.fromJson(Map<String, dynamic> json) => UserType(
    id: json["id"] == null ? null : json["id"],
    isActive: json["is_active"] == null ? null : json["is_active"],
    isDeleted: json["is_deleted"] == null ? null : json["is_deleted"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
    name: json["name"] == null ? null : json["name"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "is_active": isActive == null ? null : isActive,
    "is_deleted": isDeleted == null ? null : isDeleted,
    "created_at": createdAt == null ? null : createdAt.toIso8601String(),
    "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
    "name": name == null ? null : name,
  };
}
